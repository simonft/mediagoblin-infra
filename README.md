# MediaGoblin project infrastructure

The project currently has two VPS servers:

- puck.mediagoblin.org (website and issue tracker)
- wiki.mediagoblin.organise (legacy, wiki only)

These are hosted on Chris Webber's Linode account and have daily backups managed
by Linode. This repository currently relates only to puck.mediagoblin.org.

which

## puck.mediagoblin.org

This server hosts the website and issue tracker using Docker Compose and was
originally set up by Simon Fondrie-Teitler <simonft@riseup.net>. It consists of
containers running the following services:

- haproxy: load balancer
- mysql
- postfix
- rsyslog
- trac
- nginx: serving the website
- nginx/certbot: generating ssl certificates

The architecture is as follows: 

```
               HTTP LB     +-------+  Logs  +--------+
               +-----------+haproxy+-------->rsyslog |
               |           +-------+        +--------+
               |               | |
               |           HTTP| +-------------+
           +---v---+       LB  |       HTTP LB |
        +--+trac   +-+     +---v-------+    +--v-----------+
        |  +-------+ |     |certbot    |    |website       |
 email  |            |     +-----------+    +--------------+
 sending|       DB connection
        |            |
     +--v----+ +-----v-+
     |postfix| |mysql  |
     +-------+ +-------+
```

Docker Compose is running out of a Python virtual environment at
`/root/docker-compose-venv/` and the contents of this repository lives at
`/root/mediagoblin-compose/`. You need to be inside the `mediagoblin-compose`
directory to run Docker Compose commands. You'll also need to either activate the virtual
environment or call its executables directly (as below).


### Updating the repository

    $ sudo su
    $ cd ~/mediagoblin-compose
    $ git pull origin master


### MediaGoblin website

The website is a Pelican static site with a repository at
https://notabug.org/mediagoblin/mediagoblin-website/. To update the live site,
push changes to this repository and then run:

    $ sudo su
    $ cd ~/mediagoblin-compose
    $ /root/docker-compose-venv/bin/docker-compose build --no-cache website
    $ /root/docker-compose-venv/bin/docker-compose up --detach website
    

### Updating HAProxy with configuration changes

    $ sudo su
    $ cd ~/mediagoblin-compose
    $ git pull origin master
    $ /root/docker-compose-venv/bin/docker-compose build --no-cache haproxy
    $ /root/docker-compose-venv/bin/docker-compose up --detach haproxy

(Running `docker-compose stop/start` doesn't create new containers.)


### Adding an admin user to Trac

Run the following, where `[USERNAME]` is the username of an existing account
registered on Trac::

    $ sudo su
    $ cd /root/mediagoblin-compose
    $ /root/docker-compose-venv/bin/docker-compose run trac bash
    docker$ /var/lib/tracenv/bin/trac-admin /var/lib/trac permission add [USERNAME] TRAC_ADMIN


### Troubleshooting

In the past Trac has had persistent problems with missing email notifications and
spam registrations/broken rate limiting which meant that registrations had to be
disabled. Both these issues are now resolved and Trac appears to be working
properly. IP addresses are now passed through to track to allow it's spam
protection to work properly and HAProxy now throttles access to minimise the
load from crawlers.

Instability is an ongoing problem though and the Trac and website Docker
containers seem to go offline occasionally, requiring intervention. This is
unnecessarily frustrating for us and discouraging for contributors and
supporters.

Trac currently has a cron job that runs every hour to start its Docker
container, but this doesn't always solve the problem.

Often restarting all the Docker Compose containers will fix any persistent outages:

    $ sudo su
    $ cd ~/mediagoblin-compose
    $ /root/docker-compose-venv/bin/docker-compose restart

Occasionally that doesn't work and a `sudo reboot` is the simplest fix. Not
ideal though.


### Monitoring

Ben currently has basic HTTP monitoring set up to email him when the website or
issue tracker are down.


### Sysadmin log

**2020-02-14 Fri** mediagoblin.org certificate expired. Tried a few things; not
exactly sure what did the trick. Was fixed after running the following on
puck.mediagoblin.org:

    $ sudo su
    $ cd ~/mediagoblin-compose
    $ /root/docker-compose-venv/bin/docker-compose restart haproxy

but before that I also ran through the website deployment below.

**2019-10-28 Mon** The Trac Docker container is a little flakey but doesn't have
a process supervisor. I add a crontab -e for root that attempts to start the
container:

    */1 * * * * cd /root/mediagoblin-compose/ && /root/docker-compose-venv/bin/docker-compose start trac
