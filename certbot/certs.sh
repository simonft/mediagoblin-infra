#!/bin/bash

DIR="/etc/letsencrypt/live/"

DOMAINS=("issues.mediagoblin.org,issues-dev.mediagoblin.org" "mediagoblin.org,www.mediagoblin.org,dev.mediagoblin.org")

for DOMAIN in ${DOMAINS[@]};
do
    /.venv/bin/certbot certonly -n --webroot --webroot-path /usr/share/nginx/html/ --domains "${DOMAIN}"
done

for DOMAIN in $(find ${DIR} -iname fullchain.pem -exec dirname {} \; | xargs -n1 basename)
do
    cat "${DIR}${DOMAIN}/privkey.pem" "${DIR}${DOMAIN}/fullchain.pem" > "${DIR}${DOMAIN}.pem"
done
